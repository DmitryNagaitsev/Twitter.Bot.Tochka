﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Runtime.CompilerServices;
using Tweetinvi;
using Tweetinvi.Core.Extensions;
using Tweetinvi.Models;

namespace TochkaTwitterBot
{
    internal class Application
    {

        private static void Main()
        {
            Console.WriteLine("Введите имя пользователя Twitter");
            var login = Console.ReadLine();
            if (login.IsNullOrEmpty())
                Environment.Exit(0);
            //На самом деле нижеследующие данные должны быть скрыты от кого либо, и доступ к ним должен
            //осуществляться, например, из базы, но это же тестовое задание.
            Auth.ApplicationCredentials = new TwitterCredentials("BwigA2jcrXFPFSD8NxXRO0qp5", "P3PQvFsbFIuMtGBBulcdi0bJfEyf6FTg81HPn6xN3BcdoWQ2e5", "763381280474075137-ovlxriOO6c9Ax2fl36aqPUHRi2GXlXK", "lBuuZRXaIV9Qw9TNYw263NXgw97WNVpGY2HBL0IU6Y0ln");
            try
            {
                var user = User.GetUserFromScreenName(login);
                if (user == null)
                    throw new Exception("This user doesn't exist, try again!");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Main();
            }

            var result = ParseTwitts(login);
            Console.WriteLine(result);
            try
            {
                var reply = Tweet.PublishTweet(result);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Main();
            }
        }

        private static string ParseTwitts(string login)
        {
            var symbols = new Dictionary<char, int>();
            IEnumerable<ITweet> tweets = null;
            try
            {
                tweets = Timeline.GetUserTimeline(login, 5);
                if (tweets == null)
                    throw new Exception("this user doesn't have tweets yet!");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Main();
            }
            StringBuilder sb = new StringBuilder();
            foreach (var tweet in tweets)
            {
                sb.Append(tweet.Text);
            }
            var dict = sb.ToString().ToLookup(x => x);
            var maxCount = dict.Where(x => char.IsLetter(x.Key)).Max(x => x.Count());
            var charsArray = dict.Where(x => x.Count() == maxCount).Select(x => x.Key).ToArray();

            var ending = charsArray.Count() > 1 ? "ами" : "ой";
            sb = new StringBuilder($"{login} чаще всего пользуется букв{ending} \"{charsArray[0]}\"");
            for (var i = 1; i < charsArray.Count(); ++i)
            {
                sb.Append($" и \"{charsArray[i]}\"");
            }
            sb.Append($": {maxCount} раз за 5 твиттов!");
            return sb.ToString();
        }
    }
}
